<!DOCTYPE html>

<html>
<head>
  <title>Pdoqt</title>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <link rel="stylesheet" media="all" href="public/stylesheets/normalize.css" />
  <link rel="stylesheet" media="all" href="docco.css" />
</head>
<body>
  <div class="container">
    <div class="page">

      <div class="header">
        
          <h1>Pdoqt</h1>
        

        
      </div>

      
        
        <p>Pdoqt is a small wrapper around PDO<a href="http://php.net/manual/en/book.pdo.php">1</a> that makes it simple to run simple
queries, and possible to run complex ones. It offers two kinds of methods:
some that execute queries, and some that help you build queries. It doesn’t
prevent you from building or running invalid SQL; rather, it’s designed to
encourage a consistent and clear style of writing queries in PHP.</p>

        
          <div class='highlight'><pre>
<span class="hljs-keyword">namespace</span> <span class="hljs-title">Pdoqt</span>;

<span class="hljs-class"><span class="hljs-keyword">class</span> <span class="hljs-title">Pdoqt</span> </span>{
    <span class="hljs-keyword">use</span> \<span class="hljs-title">Defphp</span>\<span class="hljs-title">Ex</span>;

    <span class="hljs-keyword">private</span> $conn;

    <span class="hljs-keyword">public</span> <span class="hljs-function"><span class="hljs-keyword">function</span> <span class="hljs-title">__construct</span> <span class="hljs-params">(\PDO $connection)</span> </span>{
        <span class="hljs-keyword">$this</span>-&gt;conn = $connection;
    }

    <span class="hljs-keyword">public</span> <span class="hljs-function"><span class="hljs-keyword">function</span> <span class="hljs-title">getConn</span> <span class="hljs-params">()</span>: \<span class="hljs-title">PDO</span> </span>{
        <span class="hljs-keyword">return</span> <span class="hljs-keyword">$this</span>-&gt;conn;
    }</pre></div>
        
      
        
        <p>The first thing to do is to create an error-checking wrapper around the
query method. In my experience, it’s usually expected that database
statements execute successfully. An error with them is truly an
<em>exception</em> and suitable for the try/catch treatment.</p>
<p>While we’re at it, we can combine parameterized statements and
parameterless statements in a single method.</p>

        
          <div class='highlight'><pre>
    <span class="hljs-keyword">public</span> <span class="hljs-function"><span class="hljs-keyword">function</span> <span class="hljs-title">q</span> <span class="hljs-params">(string $query, array $params = null)</span>: \<span class="hljs-title">PDOStatement</span> </span>{
        <span class="hljs-keyword">if</span> (<span class="hljs-keyword">isset</span>($params)) {
            $stmt = <span class="hljs-keyword">$this</span>-&gt;conn-&gt;prepare($query);
            $stmt-&gt;execute($params);
        } <span class="hljs-keyword">else</span> {
            $stmt = <span class="hljs-keyword">$this</span>-&gt;conn-&gt;query($query);
        }

        <span class="hljs-keyword">if</span> ($stmt === <span class="hljs-keyword">false</span>) {
            $msg = <span class="hljs-string">"Error executing query $query"</span>;
            <span class="hljs-keyword">if</span> (<span class="hljs-keyword">isset</span>($params)) {
                $msg .= <span class="hljs-string">" w/ params "</span> . json_encode($params);
            }
            $msg .= <span class="hljs-string">": "</span> . implode(<span class="hljs-string">" "</span>, <span class="hljs-keyword">$this</span>-&gt;conn-&gt;errorInfo());
            <span class="hljs-keyword">static</span>::ex(<span class="hljs-string">'DbQueryError'</span>, $msg, <span class="hljs-keyword">self</span>::ERR_QUERY);
        }

        <span class="hljs-keyword">return</span> $stmt;
    }</pre></div>
        
      
        
        <p>Such a simple wrapper already makes for some fairly easy-to-read code, e.g.:</p>
<pre><code><span class="hljs-keyword">try</span> {
    $db-&gt;q(<span class="hljs-string">"CREATE TABLE foo (id INT NOT NULL, name TEXT NOT NULL)"</span>);
    $db-&gt;q(<span class="hljs-string">"INSERT INTO foo (id, name) VALUES (?, ?)"</span>, [<span class="hljs-number">1</span>, <span class="hljs-string">"Foo One"</span>]);
    $foo = $db-&gt;q(<span class="hljs-string">"SELECT * FROM foo WHERE id = :id"</span>, [<span class="hljs-string">":id"</span> =&gt; <span class="hljs-number">1</span>])-&gt;fetch();
    <span class="hljs-keyword">echo</span> json_encode($foo) . <span class="hljs-string">"\n"</span>;
} <span class="hljs-keyword">catch</span> (\<span class="hljs-keyword">Exception</span> $e) {
    error_log($e);
    <span class="hljs-keyword">exit</span>($e-&gt;getCode());
}
</code></pre><p>It also has the advantage of being very general: you can use it to run
any statement that your database server handles.</p>
<p>In most cases though, we want to run select queries. Let’s write a method
that does just that:</p>

        
          <div class='highlight'><pre>
    <span class="hljs-keyword">public</span> <span class="hljs-function"><span class="hljs-keyword">function</span> <span class="hljs-title">fetchAll</span> <span class="hljs-params">(
        string $query,
        array $params = null,
        string $indexBy = null
    )</span>: <span class="hljs-title">array</span> </span>{
        $stmt = <span class="hljs-keyword">$this</span>-&gt;q($query, $params);

        $rows = [];
        <span class="hljs-keyword">while</span> ($row = $stmt-&gt;fetch(\PDO::FETCH_ASSOC)) {
            <span class="hljs-keyword">if</span> (<span class="hljs-keyword">isset</span>($indexBy)) {
                <span class="hljs-keyword">$this</span>-&gt;indexRowByCol($rows, $row, $indexBy);
            } <span class="hljs-keyword">else</span> {
                $rows[] = $row;
            }
        }

        <span class="hljs-keyword">return</span> $rows;
    }</pre></div>
        
      
        
        <p>Notice that <code>fetchAll</code> takes the same arguments as <code>q</code>. This makes the
codebase easier to read (repeated patterns are easier to recognize).
Well, almost. There’s an optional third argument to fetchAll that lets you
index the result set by one of the columns, e.g.</p>
<pre><code>$q = <span class="hljs-string">"INSERT INTO foo (name) VALUES ('Herp', 'Derp') RETURNING name"</span>;
$rows = $db-&gt;fetchAll($q, <span class="hljs-keyword">null</span>, <span class="hljs-string">"name"</span>);
$derp = $rows[<span class="hljs-string">"Derp"</span>];
</code></pre><p>It only works if the index column has no null or duplicate values;
typically you’d use this for id columns.</p>

        
          <div class='highlight'><pre>
    <span class="hljs-keyword">private</span> <span class="hljs-function"><span class="hljs-keyword">function</span> <span class="hljs-title">indexRowByCol</span> <span class="hljs-params">(array &amp;$index, array $row, string $col)</span> </span>{
        <span class="hljs-keyword">if</span> (!<span class="hljs-keyword">isset</span>($row[$col])) {
            <span class="hljs-keyword">static</span>::ex(<span class="hljs-string">'DbIdx,DbIdxMissingCol'</span>,
                <span class="hljs-string">"No value at column '$col' for row "</span> . json_encode($row),
                <span class="hljs-keyword">self</span>::ERR_IDX_MISSING_COL);
        }
        <span class="hljs-keyword">if</span> (<span class="hljs-keyword">isset</span>($index[$row[$col]])) {
            <span class="hljs-keyword">static</span>::ex(<span class="hljs-string">'DbIdx,DbIdxDuplicateCol'</span>,
                <span class="hljs-string">"column $col isn't unique; found duplicate value "</span> .
                    json_encode($row[$col]) . <span class="hljs-string">" in row "</span> . json_encode($row),
                <span class="hljs-keyword">self</span>::ERR_IDX_DUPLICATE_COL);
        }
        $index[$row[$col]] = $row;
    }</pre></div>
        
      
        
        <p>It’s inneficient to lookup the index for every row, but then again so is
fetching an entire result set into an array. <code>fetchAll</code> should only be
used for small result sets. When expecting a lot of data, POD’s <code>fetch</code>
can be called directly to process rows one at a time, e.g.</p>
<pre><code> $stmt = $db-&gt;q(<span class="hljs-string">"SELECT * FROM foobar"</span>);
 <span class="hljs-keyword">while</span> ($row = $stmt-&gt;fetch()) {
     <span class="hljs-comment">// do something with $row</span>
 }
</code></pre><p>When you expect to get a single row (e.g. when querying by id), you can use
the <code>fetchOne</code> method, which is just <code>fetchAll</code> plus sanity checks:</p>

        
          <div class='highlight'><pre>
    <span class="hljs-keyword">public</span> <span class="hljs-function"><span class="hljs-keyword">function</span> <span class="hljs-title">fetchOne</span> <span class="hljs-params">(string $query, array $params = null)</span>: <span class="hljs-title">array</span> </span>{
        $rows = <span class="hljs-keyword">$this</span>-&gt;fetchAll($query, $params);
        <span class="hljs-keyword">if</span> (count($rows) != <span class="hljs-number">1</span>) {
            $msg = <span class="hljs-string">"Expected exactly one row but got "</span> . count($rows) .
                <span class="hljs-string">" from query $query"</span>;
            <span class="hljs-keyword">if</span> (<span class="hljs-keyword">isset</span>($params)) {
                $msg .= <span class="hljs-string">" w/ params "</span> . json_encode($params);
            }
            <span class="hljs-keyword">if</span> (count($rows)) {
                <span class="hljs-keyword">static</span>::ex(<span class="hljs-string">'DbUnexpectedRows,DbTooManyRows'</span>,
                    $msg,
                    <span class="hljs-keyword">self</span>::ERR_TOO_MANY_ROWS);
            } <span class="hljs-keyword">else</span> {
                <span class="hljs-keyword">static</span>::ex(<span class="hljs-string">'DbUnexpectedRows,DbNoRows,NotFound'</span>,
                    $msg,
                    <span class="hljs-keyword">self</span>::ERR_NO_ROWS);
            }
        }

        <span class="hljs-keyword">return</span> reset($rows);
    }</pre></div>
        
      
        
        <p>For insert, update, and delete queries, it’s often useful to get a count
of the affected rows.</p>

        
          <div class='highlight'><pre>
    <span class="hljs-keyword">public</span> <span class="hljs-function"><span class="hljs-keyword">function</span> <span class="hljs-title">nAffected</span> <span class="hljs-params">(string $query, array $params = null)</span>: <span class="hljs-title">int</span> </span>{
        <span class="hljs-keyword">return</span> <span class="hljs-keyword">$this</span>-&gt;q($query, $params)-&gt;rowCount();
    }</pre></div>
        
      
        
        <p>Calling something like</p>
<pre><code>$n = $db-&gt;nAffected(<span class="hljs-string">"DELETE FROM foo WHERE id = ?"</span>, [<span class="hljs-number">1</span>]);
</code></pre><p>doesn’t save much typing over e.g.</p>
<pre><code>$n = $db-&gt;q(<span class="hljs-string">"DELETE FROM foo WHERE id = ?"</span>, [<span class="hljs-number">1</span>])-&gt;rowCount();
</code></pre><p>but it can be easier to read. It follows the same pattern as <code>fetchAll</code>
and <code>fetchOne</code>.</p>

        
      
        
        <p>Having these wrappers around query execution doesn’t help with building
queries, but it does encourage a consistent style of interaction with
the database. Because we want to be writing SQL directly, we can’t avoid
concatenating strings. The best we can do is to give ourselves tools to
do it conveniently, and to rely on codestyle / patterns of writing to
keep things neat and clear.</p>
<p>One such tool is a helper that escapes column names. It always needs to
be done; we might as well have a consistent way of doing it.</p>

        
          <div class='highlight'><pre>
    <span class="hljs-comment">/**
     * Escape column names for use in queries.
     * For example:
     *
     *      $cols = ['a', 'b', 'c'];
     *      $q = "INSERT INTO t (" . $this-&gt;cs($cols) . ") VALUES ...";
     *
     * If an element's key is not numeric, its key is used as the column name.
     * That is, the following gives the same result as the above example:
     *
     *      $cols = ['a', 'b' =&gt; 'foo', 'c'];
     *      $q = "INSERT INTO t (" . $this-&gt;cs($cols) . ") VALUES ...";
     */</span>
    <span class="hljs-keyword">public</span> <span class="hljs-function"><span class="hljs-keyword">function</span> <span class="hljs-title">cs</span> <span class="hljs-params">(array $cols)</span>: <span class="hljs-title">string</span> </span>{
        $cs = [];
        <span class="hljs-keyword">foreach</span> ($cols <span class="hljs-keyword">as</span> $k =&gt; $v) {
            $col = is_numeric($k) ? $v : $k;
            $cs[] = <span class="hljs-string">'"'</span> . strtr($col, [<span class="hljs-string">'"'</span> =&gt; <span class="hljs-string">'""'</span>]) . <span class="hljs-string">'"'</span>;
        }

        <span class="hljs-keyword">return</span> implode(<span class="hljs-string">', '</span>, $cs);
    }</pre></div>
        
      
        
        <p>This next tool is to help us write INSERT statements. Typically, when
you’re setting out to insert records into the database, you’ll have them
in key-value arrays. This method turns those arrays into a VALUES
literal:</p>

        
          <div class='highlight'><pre>
    <span class="hljs-comment">/**
     * Generate parameters and their placeholders for use in VALUES commands.
     * For example,
     *
     *      $foobars = [
     *          ['foo' =&gt; 1, 'bar' =&gt; 2],
     *          ['foo' =&gt; 1, 'bar' =&gt; 3],
     *      ];
     *      $cols = ['foo', 'bar'];
     *      list ($vals, $params) = $this-&gt;vals($foobars, $cols);
     *      $q = "INSERT INTO foobars (" . $this-&gt;cs($cols) . ") VALUES " . $vals .
     *          " RETURNING id";
     *      $this-&gt;fetchAll($q, $params);
     *
     * Constant values can be set by using a key =&gt; value syntax for $cols.
     * That is, the following gives the same result as the above example:
     *
     *      $foobars = [
     *          ['bar' =&gt; 2],
     *          ['bar' =&gt; 3],
     *      ];
     *      $cols = ['foo' =&gt; 1, 'bar'];
     *      list ($vals, $params) = $this-&gt;vals($foobars, $cols);
     *      $q = "INSERT INTO foobars (" . $this-&gt;cs($cols) . ") VALUES " . $vals .
     *          " RETURNING id";
     *      $this-&gt;fetchAll($q, $params);
     */</span>
    <span class="hljs-keyword">public</span> <span class="hljs-function"><span class="hljs-keyword">function</span> <span class="hljs-title">vals</span> <span class="hljs-params">(array $rows, array $cols)</span>: <span class="hljs-title">array</span> </span>{
        $vals = [];
        $params = [];

        <span class="hljs-keyword">foreach</span> ($rows <span class="hljs-keyword">as</span> $row) {
            $vs = [];
            <span class="hljs-keyword">foreach</span> ($cols <span class="hljs-keyword">as</span> $k =&gt; $c) {
                <span class="hljs-keyword">if</span> (is_numeric($k)) {
                    $vs[] = <span class="hljs-string">'?'</span>;
                    $params[] = <span class="hljs-keyword">isset</span>($row[$c]) ? $row[$c] : <span class="hljs-keyword">null</span>;
                } <span class="hljs-keyword">else</span> {
                    $vs[] = $c;
                }
            }
            $vals[] = <span class="hljs-string">'('</span> . implode(<span class="hljs-string">', '</span>, $vs) . <span class="hljs-string">')'</span>;
        }

        <span class="hljs-keyword">return</span> [implode(<span class="hljs-string">",\n"</span>, $vals), $params];
    }</pre></div>
        
      
        
        <p>That’s all we need for now. You’re welcome to add more tools to this kit
along with style examples as you come accross recurring patterns.</p>

        
          <div class='highlight'><pre>
    <span class="hljs-keyword">const</span> ERR_QUERY             = <span class="hljs-number">1969001</span>;
    <span class="hljs-keyword">const</span> ERR_IDX_MISSING_COL   = <span class="hljs-number">1969002</span>;
    <span class="hljs-keyword">const</span> ERR_IDX_DUPLICATE_COL = <span class="hljs-number">1969003</span>;
    <span class="hljs-keyword">const</span> ERR_TOO_MANY_ROWS     = <span class="hljs-number">1969004</span>;
    <span class="hljs-keyword">const</span> ERR_NO_ROWS           = <span class="hljs-number">1969005</span>;
}

<span class="hljs-comment">/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */</span></pre></div>
        
      
      <div class="fleur">h</div>
    </div>
  </div>
</body>
</html>
